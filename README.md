# FORESEE: a Tool for the Systematic Comparison of Translational Drug Response Modeling Pipelines

The uniFied translatiOnal dRug rESponsE prEdiction platform FORESEE is an R-package that is designed to act as a scaffold in developing and benchmarking translational drug sensitivity models. The package is generally geared to utilize drug sensitivity knowledge gained in cancer cell line modeling to predict clinical therapy outcome in patients. For this purpose, FORESEE introduces a uniform data format for public cell line and patient data sets on the one hand and incorporates state-of-the-art preprocessing methods, model training algorithms and different validation techniques on the other hand. The modular implementation of these different functional elements offers the training and testing of diverse combinatorial models, which can be used to re-evaluate and improve already existing modeling pipelines, but also to develop new ones.


### Table of Contents
**[Installation](#installation)**<br>
**[License](#license)**<br>
**[Usage Instructions](#usage-instructions)**<br>
**[Test Environments](#test-environments)**<br>

### Installation

The easiest way to install FORESEE is via <a href="https://cran.r-project.org/web/packages/remotes/">remotes</a> package.
After installing remotes from cran, you can install FORESEE by:
```r
remotes::install_gitlab("jrc-combine/foresee", host="git.rwth-aachen.de")
```
After finishing the FORESEE installation, we recommend running
```r
installAllDependencies()
```
to install all dependencies of FORESEE. Although not necessary, this will prevent
any interruption in running the package.

### License
Distributed under GNU General Public License (GPL) v3.0. See the accompanying [license](https://github.com/JRC-COMBINE/FORESEE/blob/master/LICENSE) file or the copy at https://www.gnu.org/licenses/gpl-3.0.html.

### Usage Instructions

The package contains a vignette to explain the preparation of data contained in the package accessible via
```r
vignette(topic="DataOverview")
```
and a vignette to explain some of the functional routines that can be implemented using FORESEE accessible via 
```r
vignette(topic="introduction-to-FORESEE")
```

You can additionally access the vignettes mentioned above in your browser by:
```r
browseVignettes(package = "FORESEE")
```
